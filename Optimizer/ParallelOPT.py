#!/usr/bin/env python

import numpy as np
import cma
import dask
import pickle
import datetime
import sys, termios, tty, os, time
from dask.distributed import Client, progress
from dask_jobqueue import PBSCluster
from os import path, remove, rename



class ParallelOPT(object):
    """
    A class to optimize parameters for a cost function

    ...

    Attributes
    ----------
    cost_function : dask delayed function
        Function used to evaluate fitness. Its paramaters should be a single list of float.
    method : str
        Algorithm used to optimize the cost function
    initial_guess : tuple of float
        Initial points to be evaluated by the algorithm
    initial_sigma : float
        Initial set standart deviation. For general purposes, use the largest initial_guess
    opts : dictionary
        Optional parameters

    Methods
    -------
    optimize():
        Starts optimization
    """


    def __init__(self, cost_function, method, initial_guess, initial_sigma, *opts):
        self.cost_function = dask.delayed(cost_function)
        self.method = method
        self.initial_guess = initial_guess
        self.initial_sigma = initial_sigma
        """
        Constructs all the necessary attributes for the ParallelOPT object.

        Parameters
        ----------
            cost_function : function
                Function used to evaluate fitness. Its paramaters should be a single list of float.
            method : str
                Algorithm used to optimize the cost function
            initial_guess : tuple of float
                Initial points to be evaluated by the algorithm
            initial_sigma : float
                Initial set standart deviation. For general purposes, use the largest initial_guess
            opts : dictionary
                Optional parameters
        """

        self.opts = {
            "schedulerip": "127.0.0.1",
            "schedulerport": "8786",
            "cluster": False,
            "cores": 2,
            "processes": 2,
            "popsize": 6,
            "saveperiod" : 2, # Saves optimization each popsize x saveperiod evaluations
            "upperbound" : - np.inf,
            "lowerbound" : - np.inf,
            "backupname" : 'saved-' + method + '-object',
            "displayperiod": 100
        }
        for lists in opts:
            for opt in lists:
                locals()[opt] = lists[opt]
        for opt in self.opts:
            if opt in locals():
                self.opts[opt] = locals()[opt]
        
                
    def optimize(self):
        """
        Launches the selected method
        """
        if self.method == "cma":
            self.optimize_cma()
        else:
            print("Method not supported")
        
    
    def optimize_cma(self):
        """
        Launches the CMA-ES algorithm
        """
            
        def init_cma():
            """
            Configures Dask client and loads important files
            
            Returns
            -------
            es : CMAEvolutionStrategy object
                Object respontible for executing CMA-ES optimization
            """
            if self.opts["cluster"]:
                self.cluster = PBSCluster(header_skip=['select='],
                     cores=self.opts["cores"],
                     processes=self.opts["processes"],
                     memory="128GB",
                     project='ParallelOPT',
                     walltime='12:00:00')
                self.jobs = 2
                check_job_number()
                self.cluster.scale(jobs=self.jobs)
                client = Client(self.cluster)
            else:
                client = Client(self.opts["schedulerip"] + ':' + self.opts["schedulerport"], timeout='5s')
            if not path.exists("stop.txt"):
                f = open("stop.txt", "w")
                f.write("false")
                f.close()
            if path.exists("saved-cma-object.pkl"):
                es = pickle.load(open('saved-cma-object.pkl', 'rb'))
            else:
                f = open("log.txt", "a")
                f.write("Work begun at " + datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S") + ":\n")
                es = cma.CMAEvolutionStrategy(self.initial_guess, self.initial_sigma, 
                                              {'popsize': self.opts["popsize"],
                                               'bounds': [self.opts["lowerbound"], self.opts["upperbound"]]})
            return es
        
        def write_log(X, res):
            """
            Writes log file on each iteration
            """
            best = np.argmin(res)
            f = open("log.txt", "a")
            f.write(str(X[best]))
            f.write(" " + str(res[best]) + "\n")
            f.close()
            
    
        def write_results():
            """
            Write formated final results file
            """
            f = open("Results.txt", "a")
            f.write("Work done at " + datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S") + ":\n")
            f.write("Best solution: [" + ', '.join(str(e) for e in es.result[0]) + "]\n")
            f.write("Fitness: " + str(es.result[1]) + "\n")
            f.write("Best solution eval number: " + str(es.result[2]) + "\n")
            f.write("Total evaluations: " + str(es.result[3]) + "\n")
            f.write("Iterations: " + str(es.result[4]) + "\n")
            f.write("Stop condition: " + ''.join(str(e) for e in es.result[7]) + "\n\n\n")
            f.close()

        def finish_cma():
            """
            Removes backup files and finishes optimization
            """
            remove(self.opts["backupname"] + ".pkl.bak")
            if path.exists(self.opts["backupname"] + "_finished.pkl"):
                remove(self.opts["backupname"] + "_finished.pkl")
            rename(self.opts["backupname"] + ".pkl", self.opts["backupname"] + "_finished.pkl")
            print("Program finished")
            
        def check_stop():
            """
            Checks if the stop file txt was set to true
            """
            f = open("stop.txt", "r")
            if f.read() == "true":
                f.close()
                f = open("stop.txt", "w")
                f.write("false")
                f.close()
                return True
            f.close()
            return False


        def check_job_number():
            if path.exists("jobs.txt"):
                f = open("jobs.txt", "r")
                newjobs = int(f.read())
                if newjobs != self.jobs:
                    self.jobs = newjobs
                    self.cluster.scale(jobs=self.jobs)
                f.close()

        es = init_cma()
        #while not es.stop():
        while True:
            X = es.ask()
            res = []
            for x in X:
                res.append(self.cost_function(x))
            res = dask.compute(res)[0]
            es.tell(X, res)
            es.disp(self.opts["displayperiod"]) # Displays info on stdout
            if not (es.result[4] % self.opts["saveperiod"]):
                pickle.dump(es, open(self.opts["backupname"] + '.pkl.bak', 'wb'))
                pickle.dump(es, open(self.opts["backupname"] + '.pkl', 'wb'))
            if check_stop():
                break
            write_log(X, res) # Salvar todos as simulações (um arquivo por iteração)
        write_results()
        finish_cma()
