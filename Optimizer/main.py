import cma
import subprocess
import numpy as np
import os
import shutil
import json
import numpy as np
from math import inf
from pathlib import Path
import matplotlib.pyplot as plt
#from keyframe_page_handler import keyframe_page_handler
from ParallelOPT import ParallelOPT

class keyframe_page_handler:

    def __init__(self, file_name, parallelnumber):
        if parallelnumber == -1:
                self.file = open('configuration/core/control/keyframe/kick/front_kick/' + file_name + '.json', 'r+')
        else:
                self.file = open('simulation_%d/configuration/core/control/keyframe/kick/front_kick/' %parallelnumber + file_name + '.json', 'r+')
        self.file.seek(0)
        self.page = json.loads(self.file.read())
        self.name = file_name
        self.optimizable_params = ["leftShoulderPitch","leftShoulderYaw","leftArmRoll","leftHipYawpitch","leftHipRoll",
        "leftHipPitch","leftKneePitch","leftFootPitch","leftFootRoll","rightShoulderPitch","rightShoulderYaw","rightArmRoll",
        "rightHipYawpitch","rightHipRoll","rightHipPitch","rightKneePitch","rightFootPitch","rightFootRoll","delta"]
        self.speedrate = self.page['speedRate']
        self.interpolator = self.page['interpolator']
        self.steps = self.page['steps']
        self.numsteps = len(self.steps)
    
    def read_params(self):
        params = []
        for step in self.steps:
            for param in step:
                if param in self.optimizable_params:
                    params.append(step[param])
        return params

    def write_params(self, new_params):
        new_steps = []
        for i in range(self.numsteps):
            step = {'neckYaw' : 0.0, 'neckPitch' : 0.0}
            for j in range(3):
                index = i*len(self.optimizable_params) + j
                step[self.optimizable_params[j]] = new_params[index]
            step['leftArmYaw'] = 0.0
            for j in range(3,12):
                index = i*len(self.optimizable_params) + j
                step[self.optimizable_params[j]] = new_params[index]
            step['rightArmYaw'] = 0.0
            for j in range(12,19):
                index = i*len(self.optimizable_params) + j
                step[self.optimizable_params[j]] = new_params[index]
            new_steps.append(step)
        new_keyframe = {'name' : self.name,
        'speedRate' : self.speedrate,
        'interpolator' : self.interpolator,
        'steps' : new_steps}
        self.file.seek(0)
        self.file.truncate(0)
        self.file.write(json.dumps(new_keyframe))
        self.file.flush()

def write_keyframe(x, parallelnumber, prep, back, front):
    index = 0
    prep.write_params(x[index:index+prep.numsteps*19])
    index += prep.numsteps*19
    back.write_params(x[index:index+back.numsteps*19])
    index += back.numsteps*19
    front.write_params(x[index:index+front.numsteps*19])
    return True

def evaluate_new_keyframe(x):
    parallelnumber = np.random.randint(10000)
    while os.path.exists("simulation_%d" %parallelnumber):
        parallelnumber = np.random.randint(10000)
    while True:
        try:
            os.mkdir("simulation_%d" %parallelnumber)
            break
        except FileExistsError:
            parallelnumber = np.random.randint(10000)
    os.mkdir("simulation_%d/binaries" %parallelnumber)
    os.symlink("../Optimizer/libs", "simulation_%d/libs" %parallelnumber)
    shutil.copy2("Optimizer/binaries/KickKeyframeOptimization_AgentTest", "simulation_%d/binaries/KickKeyframeOptimization_AgentTest" %parallelnumber)
    shutil.copytree("Optimizer/configuration", "simulation_%d/configuration" %parallelnumber)
    prep = keyframe_page_handler('prepFrontKickFast', parallelnumber)
    back = keyframe_page_handler('moveLegBack', parallelnumber)
    front = keyframe_page_handler('moveLegFront', parallelnumber)
    write_keyframe(x, parallelnumber, prep, back, front)
    errcounter = 0
    server_speed_locked = False
    #max_time = 150 if server_speed_locked else 12
    max_time = 150 if server_speed_locked else 24
    while True:
        try:
            subprocess.Popen(['bash', 'start_rcssserver3d.sh', '%d' %(10000+parallelnumber), '%d' %(20000+parallelnumber), '%d' %(parallelnumber)], cwd='soccer3d/', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            subprocess.run('sleep 3', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            subprocess.run(['./KickKeyframeOptimization_AgentTest', '%d' %(parallelnumber)], cwd='simulation_%d/binaries/' %parallelnumber, stdout=subprocess.PIPE, timeout=max_time)
            pid = open('simulation_%d/pid.txt' %parallelnumber, 'r')
            subprocess.run('kill -9 %d' %int(pid.readline()), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            interface = open('simulation_%d/binaries/interface.txt' %parallelnumber, 'r')
            fitness = interface.readline()
            interface.close()
            break
        except subprocess.TimeoutExpired:
            pid = open('simulation_%d/pid.txt' %parallelnumber, 'r')
            subprocess.run('kill -9 %d' %int(pid.readline()), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            errcounter = errcounter + 1
            if errcounter == 3:
                fitness = -5
                break
        except FileNotFoundError:
            pid = open('simulation_%d/pid.txt' %parallelnumber, 'r')
            subprocess.run('kill -9 %d' %int(pid.readline()), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            errcounter = errcounter + 1
            if errcounter == 3:
                fitness = -5
                break
        except:
            pid = open('simulation_%d/pid.txt' %parallelnumber, 'r')
            subprocess.run('kill -9 %d' %int(pid.readline()), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            errcounter = errcounter + 1
            if errcounter == 3:
                fitness = -5
                break
    #print('fitness: %f'%(-1*float(fitness)))
    subprocess.run('rm -r simulation_%d' %parallelnumber, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    subprocess.run('sleep 2', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    subprocess.run('rm -r simulation_%d' %parallelnumber, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    subprocess.run('sleep 1', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    shutil.rmtree("simulation_%d" %parallelnumber, ignore_errors=True)
    subprocess.Popen(['bash', 'delete_simulation_folder.sh', '%d' %(parallelnumber)], cwd='Optimizer/', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return -1*float(fitness)

"""
def otimize_cmaes(function, sigma0, epsilon, m0):
    es = cma.CMAEvolutionStrategy(m0.flatten(), sigma0)
    diff = inf
    oldfitness = inf
    fitness_history = []
    best_fitness = inf
    best_sample = None

    counter = 0
    while diff > epsilon:
        counter = counter + 1
        print('Episódio %d'%counter)
        samples = es.ask()
        fitness = np.array([function(sample) for sample in samples])
        index_best = np.argmin(fitness)

        if fitness[index_best] < best_fitness:
            best_fitness = fitness[index_best]
            best_sample = samples[index_best]
            best_file = open('best_sample.txt', 'w')
            best_file.write(str(best_sample))
            best_file.close()
        
        fitness_history.extend(fitness.tolist())
        es.tell(samples, fitness)
        diff = np.max(np.absolute(fitness - oldfitness))
        oldfitness = fitness        
        plt.plot(fitness_history)
        plt.savefig('fitness.png')
        plt.clf()
             
    return fitness_history
"""

input_shape = (19, 11)
sigma0 = 0.007
function = evaluate_new_keyframe
#path = str(Path.home()) + "/RoboViz/bin/"
#subprocess.Popen('./roboviz.sh', cwd=path, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
prep0 = keyframe_page_handler('prepFrontKickFast', -1)
back0 = keyframe_page_handler('moveLegBack', -1)
front0 = keyframe_page_handler('moveLegFront', -1)
m0 = prep0.read_params()
m0.extend(back0.read_params())
m0.extend(front0.read_params())
m0 = np.array(m0)

fitnesses = ParallelOPT(function, "cma", m0.flatten(), sigma0, {'cluster': True})
#fitnesses = ParallelOPT(function, "cma", m0.flatten(), sigma0, {'popsize': 48, 'cores': 2, 'processes': 48,'cluster': True})
fitnesses.optimize()
