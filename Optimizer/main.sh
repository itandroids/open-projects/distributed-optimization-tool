#PBS -N parallelopt
cd ~/Optimizer
echo Starting calculation
source activate parallelopt
pip install --user dask dask-jobqueue
python main.py
echo End of calculation
