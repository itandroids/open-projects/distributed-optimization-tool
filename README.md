# Parallel optimization tool

This tool is a pure python library designed to accelerate computing-intensive optimization problems based on evolutionary algorithms on distributed and highly-parallel systems.

## Requirements

* [Dask](https://github.com/dask/dask) 2.5.2 (tested version)
* [Dask-jobqueue](https://github.com/dask/dask-jobqueue) 0.7.1 (tested version) [optional for PBS cluster optimization]
* [CMA-ES](https://github.com/CMA-ES/pycma) 2.7.0 (tested version) [optional for CMA-ES optimization]

## Introduction

A common problem when dealing with computing-intensive evolutionary algorithms is that it can take weeks or months to achieve a desired performance. In these cases, one alternative is computing it in the cloud or in a distributed system with parallel capacity.

While there are many great free solutions available to easily distribute tasks, they lack essential features desired when dealing with evolutionary algorithms, especially *checkpoints* and *recovery* from crashes.

This tool provides an interface that integrates [Dask](https://github.com/dask/dask) and evolutionary algorithms (currently, only CMA-ES) by distributing the fitness evaluation (cost function) of a population.

### Features

Some of the major features of this tool are:

* automatic *checkpoints* and *recovering* from crashes with rescheduling of tasks
* dynamic allocation of computing units
* interface for integration with python code for monitoring and control of the optimization and distributed system
* easy setup on scientific clusters
* easy customization for other algorithms

### How it works

It works on a scheduler - worker logic. Both must be initialized by the user. The ParallelOPT object connects to the Dask scheduler using the `ip:port` provided by the user on the configurations.
It contains only the class ParallelOPT, which has the accessible method `optimize()`.
Initiate a ParallelOPT object. It shoud be initialized with at least 4 arguments, in order:

* Cost function (fitness)
  : A function pointer. This function may only have one argument, which is a list of floating point numbers (the size will be the same as Initial guess'). It is recommended to use numpy functions to do mathematical operations. This library does not have built-in methods for timeout control. This functionality has to be handled in the Cost function.

* Method
  : A string identifying the optimization metaheuristic.

* Initial guess
  : A list of floating point numbers

* Initial sigma
  : A single floating point number

Followed by an optional dictionary of options. Check the Configurations section for more info.

To start optimization, call the method optimize(). It will create 5 files: two backup files, ending in .pkl and .pkl.bak, a stop.txt, a log.txt and a results.txt.

To stop optimization, modify the content of stop.txt to true.

A log will be created will be create in log.txt. In each row, the best parameters of a iteration will be printed in order between branches [] followed by the fitness.
The results will be print in the results.txt file. The stop condition will be a string provided by the optimization module. Check its documentation for more info.

## Configurations

These arguments should be passed as a dictionary to the ParallelOPT object upon instantiation.

List of configurations:

* `schedulerip`
  : Dask scheduler ip address string. (default: "127.0.0.1")

* `schedulerport`
  : Dask scheduler port string. (default: "8786")

* `cluster`
  : Defines if a PBS cluster should be used. (default: "8786")

* `cores`
  : Number of cores per worker

* `processes`
  : Number of processes per worker (threads are defined by dividings core/processes)

* `popsize`
  : Number of cost function calls on each iteration.
  Larger results will converge slowly, but will get better results. (default: 6)

* `saveperiod`
  : Number of iterations between each backup save. (default: 2)

* `upperbound`
  : Highest value possible to be passed to the cost function. (default: +np.inf)

* `lowerbound`
  : Lowest value possible to be passed to the cost function. (default: -np.inf)

* `backupname`
  : Name of the saved pickle object for backup purposes. (default: 'saved-' + method + '-object')

* `displayperiod`
  : Iteration period between each status display on the stdout. (default: 100)

## Examples

Here is a simple example of usage of the package:

```python
from ParallelOPT import ParallelOPT
from time import sleep
import numpy as np

def cost_function(args):
    x = args[0]
    y = args[1]
    #sleep(2)
    return np.sin(x)**10 + np.cos(10 + x*y) * np.cos(x) + 1

initial_guess = 2 * [1]
initial_sigma = 1
opt = ParallelOPT(cost_function, "cma", initial_guess, initial_sigma, {'popsize': 10, 'displayperiod': 50})

opt.optimize()
```

You can specify boundaries to the values using mathematical tricks on the cost function

```python
def bounded_cost_function(args):
    x = args[0]
    y = args[1]
    # In cost function, x is in [0,5] and y is in [6,8]
    return cost_function([2.5 + 2.5 * np.sin(x), 7 + 0.5 * np.sin(y)])

def cost_function(args):
    x = args[0]
    y = args[1]
    return np.sin(x)**10 + np.cos(10 + x*y) * np.cos(x) + 1
```

## Troubleshooting

If the program fails to load the .pkl file, delete it and rename the .pkl.bak file to .pkl

Updates in the cma package might render .pkl objects useless and cause unpredictable failures.

## Setup (cluster)

* PBS Cluster
  The required packages may not be installed in the submission server. Fix this by submitting a .sh file containing.

```bash
source activate myenv
pip install --user cma dask dask-jobqueue
```

To manage the number of jobs submitted, create a job.txt file containing the desired number of jobs.

## 3D Simulation league setup

The server files are on the soccer3d/ folder on the root of this repository.
Put both folders (Optimization and soccer3d) on the main folder of your DevCloud workspace.

Use the folders 'binaries', 'configuration' and 'libs' to setup your soccer agent.

The tool tries to delete all simulation folders upon finishing the simulations. Some folders may remain undeleted.
Delete them by running delete\_simulation\_folder.sh

