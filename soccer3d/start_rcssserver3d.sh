cd ~/soccer3d/scripts/rcssserver3d

LD_LIBRARY_PATH=~/soccer3d/libs_minimal_ubuntu:$LD_LIBRARY_PATH
LD_LIBRARY_PATH=~/soccer3d/simspark_libs_ubuntu:$LD_LIBRARY_PATH
LD_LIBRARY_PATH=~/soccer3d/simspark/lib/rcssserver3d:$LD_LIBRARY_PATH
LD_LIBRARY_PATH=~/soccer3d/simspark/lib/simspark:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH

./rcssserver3d --agent-port $1 --server-port $2 --script-path rcssserver3d.rb --init-script-prefix ~/soccer3d/simspark/share/simspark/ & #&>/dev/null

echo "$!" > ~/simulation_$3/pid.txt
